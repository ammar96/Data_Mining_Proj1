import numpy as np
import pandas as pd
from sklearn import tree

# train_url = "http://s3.amazonaws.com/assets.datacamp.com/course/Kaggle/train.csv"
# train = pd.read_csv(train_url)
# test_url = "http://s3.amazonaws.com/assets.datacamp.com/course/Kaggle/test.csv"
# test = pd.read_csv(test_url)
from sklearn.ensemble import RandomForestClassifier

train = pd.read_csv("train.csv")
test = pd.read_csv("test.csv")

train = train.assign(Child=lambda x: 0)
train["Sex"][train["Sex"] == "male"] = 0
train["Sex"][train["Sex"] == "female"] = 1
train["Age"] = train["Age"].fillna(train["Age"].median())
train["Child"][train["Age"] < 18] = 1
train["Child"][train["Age"] >= 18] = 0
train["Embarked"] = train["Embarked"].fillna("S")
train["Embarked"][train["Embarked"] == "S"] = 0
train["Embarked"][train["Embarked"] == "C"] = 1
train["Embarked"][train["Embarked"] == "Q"] = 2

test["Sex"][test["Sex"] == "male"] = 0
test["Sex"][test["Sex"] == "female"] = 1
test["Age"] = test["Age"].fillna(train["Age"].median())
test["Embarked"] = test["Embarked"].fillna("S")
test["Embarked"][test["Embarked"] == "S"] = 0
test["Embarked"][test["Embarked"] == "C"] = 1
test["Embarked"][test["Embarked"] == "Q"] = 2
test.Fare[152] = test.Fare.median()

target = train["Survived"].values
features_forest = train[["Pclass", "Age", "Sex", "Fare", "SibSp", "Parch", "Embarked"]].values
forest = RandomForestClassifier(max_depth=12, min_samples_split=5, n_estimators=10, random_state=1)
my_forest = forest.fit(features_forest, train["Survived"])
print(my_forest.score(features_forest, train["Survived"]))
test_features = test[["Pclass", "Age", "Sex", "Fare", "SibSp", "Parch", "Embarked"]].values
pred_forest = my_forest.predict(test_features)
PassengerId = (test["PassengerId"]).astype(int)
my_solution_randomforest = pd.DataFrame(pred_forest, PassengerId, columns=["Survived"])
my_solution_randomforest.to_csv("my_solution_randomforest.csv", index_label=["PassengerId"])

print("finished")
