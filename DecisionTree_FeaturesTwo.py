import numpy as np
import pandas as pd
from sklearn import tree

# train_url = "http://s3.amazonaws.com/assets.datacamp.com/course/Kaggle/train.csv"
# train = pd.read_csv(train_url)
# test_url = "http://s3.amazonaws.com/assets.datacamp.com/course/Kaggle/test.csv"
# test = pd.read_csv(test_url)

train = pd.read_csv("train.csv")
test = pd.read_csv("test.csv")

train = train.assign(Child=lambda x: 0)
train["Sex"][train["Sex"] == "male"] = 0
train["Sex"][train["Sex"] == "female"] = 1
train["Age"] = train["Age"].fillna(train["Age"].median())
train["Child"][train["Age"] < 18] = 1
train["Child"][train["Age"] >= 18] = 0
train["Embarked"] = train["Embarked"].fillna("S")
train["Embarked"][train["Embarked"] == "S"] = 0
train["Embarked"][train["Embarked"] == "C"] = 1
train["Embarked"][train["Embarked"] == "Q"] = 2

test["Sex"][test["Sex"] == "male"] = 0
test["Sex"][test["Sex"] == "female"] = 1
test["Age"] = test["Age"].fillna(train["Age"].median())
test["Embarked"] = test["Embarked"].fillna("S")
test["Embarked"][test["Embarked"] == "S"] = 0
test["Embarked"][test["Embarked"] == "C"] = 1
test["Embarked"][test["Embarked"] == "Q"] = 2
test.Fare[152] = test.Fare.median()

target = train["Survived"].values
features_two = train[["Pclass", "Sex", "Age", "Fare", "SibSp", "Parch", "Embarked"]].values
# Control overfitting by setting "max_depth" to 10 and "min_samples_split" to 5
max_depth = 10
min_samples_split = 5
my_tree_two = tree.DecisionTreeClassifier(max_depth=max_depth, min_samples_split=min_samples_split, random_state=1)
my_tree_two = my_tree_two.fit(features_two, target)
print(my_tree_two.score(features_two, target)) #Print the score of the new decison tree
tree.export_graphviz(my_tree_two, out_file="re.dot") #?
test_features = test[["Pclass", "Sex", "Age", "Fare", "SibSp", "Parch", "Embarked"]].values
my_prediction = my_tree_two.predict(test_features)
PassengerId = (test["PassengerId"]).astype(int)
my_solution = pd.DataFrame(my_prediction, PassengerId, columns=["Survived"])
my_solution.to_csv("my_solution_two.csv", index_label=["PassengerId"])

print("finished")
