import numpy as np
import pandas as pd
from sklearn import tree

# train_url = "http://s3.amazonaws.com/assets.datacamp.com/course/Kaggle/train.csv"
# train = pd.read_csv(train_url)
# test_url = "http://s3.amazonaws.com/assets.datacamp.com/course/Kaggle/test.csv"
# test = pd.read_csv(test_url)

train = pd.read_csv("train.csv")
test = pd.read_csv("test.csv")

train = train.assign(Child=lambda x: 0)
train["Sex"][train["Sex"] == "male"] = 0
train["Sex"][train["Sex"] == "female"] = 1
train["Age"] = train["Age"].fillna(train["Age"].median())
train["Child"][train["Age"] < 18] = 1
train["Child"][train["Age"] >= 18] = 0
train["Embarked"] = train["Embarked"].fillna("S")
train["Embarked"][train["Embarked"] == "S"] = 0
train["Embarked"][train["Embarked"] == "C"] = 1
train["Embarked"][train["Embarked"] == "Q"] = 2

test["Sex"][test["Sex"] == "male"] = 0
test["Sex"][test["Sex"] == "female"] = 1
test["Age"] = test["Age"].fillna(train["Age"].median())
test["Embarked"] = test["Embarked"].fillna("S")
test["Embarked"][test["Embarked"] == "S"] = 0
test["Embarked"][test["Embarked"] == "C"] = 1
test["Embarked"][test["Embarked"] == "Q"] = 2

target = train["Survived"].values
features_one = train[["Pclass", "Sex", "Age", "Fare"]].values
my_tree_one = tree.DecisionTreeClassifier()
my_tree_one = my_tree_one.fit(features_one, target)
print(my_tree_one.feature_importances_)
print(my_tree_one.score(features_one, target))

test.Fare[152] = test.Fare.median()
test_features = test[["Pclass", "Sex", "Fare", "Age"]].values
my_prediction = my_tree_one.predict(test_features)
PassengerId = np.array(test["PassengerId"]).astype(int)
my_solution = pd.DataFrame(my_prediction, PassengerId, columns=["Survived"])
print(my_solution)
print(my_solution.shape)
my_solution.to_csv("my_solution_one.csv", index_label=["PassengerId"])

print("finished")
