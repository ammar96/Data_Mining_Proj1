import numpy as np
import pandas as pd
from sklearn import tree

# train_url = "http://s3.amazonaws.com/assets.datacamp.com/course/Kaggle/train.csv"
# train = pd.read_csv(train_url)
# test_url = "http://s3.amazonaws.com/assets.datacamp.com/course/Kaggle/test.csv"
# test = pd.read_csv(test_url)

train = pd.read_csv("train.csv")
test = pd.read_csv("test.csv")

train = train.assign(Child=lambda x: 0)
train["Sex"][train["Sex"] == "male"] = 0
train["Sex"][train["Sex"] == "female"] = 1
train["Age"] = train["Age"].fillna(train["Age"].median())
train["Child"][train["Age"] < 18] = 1
train["Child"][train["Age"] >= 18] = 0
train["Embarked"] = train["Embarked"].fillna("S")
train["Embarked"][train["Embarked"] == "S"] = 0
train["Embarked"][train["Embarked"] == "C"] = 1
train["Embarked"][train["Embarked"] == "Q"] = 2

test["Sex"][test["Sex"] == "male"] = 0
test["Sex"][test["Sex"] == "female"] = 1
test["Age"] = test["Age"].fillna(train["Age"].median())
test["Embarked"] = test["Embarked"].fillna("S")
test["Embarked"][test["Embarked"] == "S"] = 0
test["Embarked"][test["Embarked"] == "C"] = 1
test["Embarked"][test["Embarked"] == "Q"] = 2
test.Fare[152] = test.Fare.median()

target = train["Survived"].values
train_two = train.copy()
train_two["family_size"] = train_two["SibSp"] + train_two["Parch"] + 1
test_two = test.copy()
test_two["family_size"] = test_two["SibSp"] + test_two["Parch"] + 1
features_three = train_two[["Pclass", "Sex", "Age", "Fare", "SibSp", "Parch", "family_size"]].values
max_depth = 10
min_samples_split = 5
my_tree_three = tree.DecisionTreeClassifier(max_depth=max_depth, min_samples_split=min_samples_split, random_state=1)
my_tree_three = my_tree_three.fit(features_three, target)
tree.export_graphviz(my_tree_three, out_file="re.dot")  # ?
test_features = test_two[["Pclass", "Sex", "Age", "Fare", "SibSp", "Parch", "family_size"]].values
my_prediction = my_tree_three.predict(test_features)
PassengerId = (test_two["PassengerId"]).astype(int)
my_solution = pd.DataFrame(my_prediction, PassengerId, columns=["Survived"])
my_solution.to_csv("my_solution_three.csv", index_label=["PassengerId"])
